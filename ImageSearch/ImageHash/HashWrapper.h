#pragma once

using namespace System;

#include "ImageFileHash.h"
#include "HashParams.h"

namespace CoreNative {

	ref class HashWrapper
	{
	public:
		HashWrapper();
		HashWrapper(HashOptions options);
		~HashWrapper();
		!HashWrapper();

		IImageHash * get();

	private:
		IImageHash *hash;
		IImageHash *hashInner;
	};

}