// CoreNativeWrapper.h

#pragma once

using namespace System;

#include "ImageFileHash.h"
#include "Hamming.h"
#include "HashParams.h"
#include "HashWrapper.h"

namespace CoreNative {

	///Wrapper for unmanaged class
	// Calculate hash for image file
	public ref class ImageHash
	{
		// TODO: Add your methods for this class here.
	public:

		ImageHash();
		ImageHash(HashOptions options);
		~ImageHash();
		!ImageHash();

		//calculate hash for image file name fn
		UInt64 calcHash(String ^fn);

	private:
		HashWrapper ^hash;
		ImageFileHash *imgFileHash;
	};
}
