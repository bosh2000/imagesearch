#pragma once

#include <opencv2/opencv.hpp>

//Template: method
//calculate image hash method
__interface IImageHash
{
	uint64_t calcHash(const cv::Mat & src);
};
