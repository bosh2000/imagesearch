#pragma once

#include <string>
#include "ImageHash.h"

///calculate image hash for file
class ImageFileHash
{
public:
	ImageFileHash(IImageHash *hash);
	~ImageFileHash();

	//calculate hash for file name fn
	bool calcHash(const std::string & fn, uint64_t &hash);

private:
	IImageHash *pImageHash;
};

