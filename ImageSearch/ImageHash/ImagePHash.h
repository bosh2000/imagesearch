#pragma once

#include "ImageHash.h"

//DCT hash
class ImagePHash : public IImageHash {
public:
	ImagePHash();

	//calculate hash for image src
	uint64_t calcHash(const cv::Mat & src);

	cv::Mat & resizeMat();
	cv::Mat & grayMat();
	cv::Mat & binMat();
	cv::Mat & dctResultMat();
private:
	cv::Mat res, gray, bin, dctInput, dctResult;

	//size of matrix
	static const cv::Size resizeSize;
	//extracted rect for subMatrix
	static const cv::Rect subMatrixRect;

	//prepare image src for calculating hash
	//filling class members res, gray, dctRes
	void prepare(const cv::Mat & src);

	//calculating hash on this->bin
	uint64_t calc();
};
