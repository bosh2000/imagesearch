#pragma once

namespace CoreNative {
	/* ��������� ���������� ��������� ����������������*/
	public value struct HashOptions
	{
		/* �������� ������������� ����������� */
		/*bool enableDenoise = false; �������� ������ ����������� �3446*/
		bool enableDenoise;
		/* �������� ����������� �����������*/
		/*bool enableBlur = false; �������� ������ ����������� �3446*/
		bool enableBlur;
	};
}