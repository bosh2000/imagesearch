#pragma once

#include "ImageHash.h"

class ImageAHash : public IImageHash
{
public:
	//constructor
	ImageAHash();

	//calculate hash for image src
	uint64_t calcHash(const cv::Mat & src);

	cv::Mat & resizeMat();
	cv::Mat & grayMat();
	cv::Mat & binMat();
private:
	cv::Mat res, gray, bin;

	//size of matrix
	static const cv::Size resizeSize;

	//prepare image src for calculating hash
	//filling class members res, gray, bin
	void prepare(const cv::Mat & src);

	//calculating hash on this->bin
	uint64_t calc();
};

