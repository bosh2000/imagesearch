#pragma once

#include "ImageHash.h"

//make image denoised and blured and apply other hash algorithm
class ImageHashBlurWrapper : public IImageHash
{
public:
	ImageHashBlurWrapper(IImageHash *hash, bool useDenoise, bool useBlur);

	uint64_t calcHash(const cv::Mat & src);

private:
	//hash algorithm
	IImageHash *hashAlg;

	bool useDenoise, useBlur;

	static const cv::Size blurSize;
	static const int denoiseLevel;

	cv::Mat gray, blured, denoised;
};

