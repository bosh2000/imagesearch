﻿#include "stdafx.h"

#include <msclr\marshal_cppstd.h>

#include "ImageHashWrapper.h"
#include "ImageAHash.h"
#include "ImagePHash.h"
#include "ImageDHash.h"

using namespace CoreNative;

ImageHash::ImageHash() {
	hash = gcnew HashWrapper();
	imgFileHash = new ImageFileHash(hash->get());
}

ImageHash::ImageHash(HashOptions options) {
	hash = gcnew HashWrapper(options);
	imgFileHash = new ImageFileHash(hash->get());
}



ImageHash::~ImageHash() {
	this->!ImageHash();
}

ImageHash::!ImageHash() {
	delete imgFileHash;
}

//calculate hash for image file name fn
UInt64 ImageHash::calcHash(String ^fn) {
	msclr::interop::marshal_context context;
	std::string s = context.marshal_as<std::string>(fn);
	uint64_t hash;
	if (imgFileHash->calcHash(s, hash))
		return hash;
	else
		throw gcnew System::IO::FileNotFoundException("May be file not found");
}