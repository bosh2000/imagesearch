#pragma once

#include "ImageHash.h"

class ImageDHash : public IImageHash
{
public:
	ImageDHash();

	//calculate hash for image src
	uint64_t calcHash(const cv::Mat & src);

	cv::Mat & resizeMat();
	cv::Mat & grayMat();
private:
	cv::Mat res, gray;

	//size of matrix
	static const cv::Size resizeSize;

	//prepare image src for calculating hash
	//filling class members res, gray, dctRes
	void prepare(const cv::Mat & src);

	//calculating hash on this->bin
	uint64_t calc();
};

