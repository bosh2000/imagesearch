#include "stdafx.h"
#include "HashWrapper.h"

#include "ImageAHash.h"
#include "ImagePHash.h"
#include "ImageDHash.h"
#include "ImageHashBlurWrapper.h"

using namespace CoreNative;

HashWrapper::HashWrapper() {
	hash = new ImageAHash();
	hashInner = NULL;
}

HashWrapper::HashWrapper(HashOptions options) {
	if (options.enableBlur || options.enableDenoise) {
		hashInner = new ImageAHash();
		hash = new ImageHashBlurWrapper(hashInner, options.enableDenoise, options.enableBlur);
	}
	else {
		hash = new ImageAHash();
		hashInner = NULL;
	}
}

HashWrapper::~HashWrapper() {
	this->!HashWrapper();
}

HashWrapper::!HashWrapper() {
	delete hash;
	if (hashInner != NULL)
		delete hashInner;
}

IImageHash * HashWrapper::get() {
	return hash;
}
