#pragma once

#include <stdint.h>

///Calculating hamming distance
// http://en.wikipedia.org/wiki/Hamming_distance
class Hamming
{
public:

	//static method calculate hamming distance between x and y
	//returns hamming distance
	static int64_t distance(uint64_t x, uint64_t y);

private:
	Hamming();
};

