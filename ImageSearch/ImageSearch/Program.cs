﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ImageSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Image> images = new List<Image>()
            {
                new Image() {Hash = 0, Uri = "c:\\Temp\\001.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\002.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\003.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\004.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\005.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\006.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\007.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\008.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\009.jpg"},
                new Image() { Hash = 0, Uri = "c:\\Temp\\010.jpg"},
            };

            Image etalonImage = new Image() {Hash = 0, Uri = "c:\\temp\\etalon.jpg"};

            CalculateHash.Calculate(images);
            CalculateHash.Calculate(etalonImage);



        }
    }
}
