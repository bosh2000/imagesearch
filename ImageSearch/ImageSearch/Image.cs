﻿using System;

namespace ImageSearch
{
    public class Image
    {
        public string Uri { get; set; }
        public UInt64 Hash;
    }
}